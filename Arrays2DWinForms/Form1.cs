﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays2DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double[,] matr;

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal n = numericUpDownRows.Value;
            int rows = (int)(n);
            decimal m = numericUpDownColumns.Value;
            int columns = (int)(m);
            matr = new double[columns, rows];
            dataGridViewMatrix.RowCount = rows;
            dataGridViewMatrix.ColumnCount = columns;
            Random rnd = new Random();
            for (int j = 0; j < matr.GetLength(1); j++)
            {
                for (int i = 0; i < matr.GetLength(0); i++)
                {
                    matr[i, j] = -28.305 + rnd.NextDouble() * (12.951 + 28.305);
                    dataGridViewMatrix[i, j].Value = Math.Round(matr[i, j], 3);
                    dataGridViewMatrix.Rows[j].HeaderCell.Value = j.ToString();
                    dataGridViewMatrix.Columns[i].HeaderText = i.ToString();
                    dataGridViewMatrix.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double min, max = double.MinValue;
            for (int i = 0; i < matr.GetLength(1); i++)
            {
                min = matr[0, i];
                for (int j = 0; j < matr.GetLength(0); j++)
                {
                    if (matr[j, i] < min)
                        min = matr[j, i];
                }
                if (min > max)
                    max = min;
            }
            textBoxMax.Text = max.ToString("F3");

            double tmp;
            int x = 0;
            for (int i = 1; i < matr.GetLength(1); i += 2)
            {
                for (int j = 0; j < matr.GetLength(0); j++)
                {
                    if (matr[j, i] < 0.0 && j == 0)
                    {
                        x++;
                        continue;
                    }
                    if (j > 0 && matr[j, i] < 0.0 && matr[j - 1, i] > 0.0)
                    {
                        for (int k = j; k > x; k--)
                        {
                            tmp = matr[k, i];
                            matr[k, i] = matr[k - 1, i];
                            matr[k - 1, i] = tmp;
                        }
                        x++;
                    }
                    if (j > 0 && matr[j, i] < 0.0 && matr[j - 1, i] < 0.0)
                        x++;
                }
                x = 0;
            }

            for (int j = 0; j < matr.GetLength(1); j++)
            {
                for (int i = 0; i < matr.GetLength(0); i++)
                {
                    dataGridViewMatrix[i, j].Value = Math.Round(matr[i, j], 3);
                }
            }
        }
        private void dataGridViewMatrix_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == -1 && e.RowIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                using (SolidBrush br = new SolidBrush(Color.Black))
                {
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(e.RowIndex.ToString(),
                      e.CellStyle.Font, br, e.CellBounds, sf);
                }
                e.Handled = true;
            }
        }
    }
}