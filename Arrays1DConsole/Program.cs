﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays1DConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Write("Лабораторна робота №3.\nВиконала: Волинець Н.С., група ПІ-53\nВаріант №3\nЗавдання 1.\n");
            Console.WriteLine("---------------------------------");
            uint n;
            bool isTrue;
            do
            {
                Console.Write("Введіть кількість елементів одновимірного масиву: ");
                isTrue = uint.TryParse(Console.ReadLine(), out n);
                if (!isTrue || n == 0)
                    Console.WriteLine("Помилка! Будь ласка, повторіть введення значення ще раз!");
            } while (!isTrue || n == 0);
            Console.WriteLine("---------------------------------");

            Console.WriteLine("Початковий масив:");
            double[] arr = new double[n];
            Random rnd = new Random();
            double sum = 0.0;
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = -15.6 + rnd.NextDouble() * (53.3 + 15.6);
                if ((Math.Abs(Math.Round(arr[i], 1)) - Math.Abs(Math.Truncate(arr[i]))) >= 0.5)
                    sum += Math.Abs(Math.Round(arr[i], 1));
                Console.WriteLine($"arr[{i}] = {Math.Round(arr[i], 1):F1}");
            }
            Console.WriteLine("---------------------------------");
            Console.WriteLine($"Сума модулів елементів масиву: {sum:F1}");
            Console.WriteLine("---------------------------------");

            double tmp;
            bool isEnabled;
            do
            {
                isEnabled = false;
                for (int i = arr.Length / 2; i < arr.Length - 1; i++)
                {
                    if (arr[i] < arr[i + 1])
                    {
                        tmp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = tmp;
                        isEnabled = true;
                    }
                }
            } while (isEnabled);

            Console.WriteLine("Відсортований масив:");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine($"arr[{i}] = {arr[i]:F1}");
            }
        }
    }
}
