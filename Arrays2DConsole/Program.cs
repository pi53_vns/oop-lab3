﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrays2DConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Write("Лабораторна робота №3.\nВиконала: Волинець Н.С., група ПІ-53\nВаріант №3\nЗавдання 2.\n");
            Console.WriteLine("------------------------------------");
            uint n, m;
            bool isTrue;
            do
            {
                Console.Write("Введіть кількість рядків матриці: ");
                isTrue = uint.TryParse(Console.ReadLine(), out n);
                if (!isTrue || n == 0)
                    Console.WriteLine("Помилка! Будь ласка, повторіть введення значення ще раз!");
            } while (!isTrue || n == 0);
            Console.WriteLine("------------------------------------");
            do
            {
                Console.Write("Введіть кількість стовпців матриці: ");
                isTrue = uint.TryParse(Console.ReadLine(), out m);
                if (!isTrue || m == 0)
                    Console.WriteLine("Помилка! Будь ласка, повторіть введення значення ще раз!");
            } while (!isTrue || m == 0);
            Console.WriteLine("------------------------------------");

            double[,] matr = new double[n, m];
            Random rnd = new Random();
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    matr[i, j] = -28.305 + rnd.NextDouble() * (12.951 + 28.305);
                    Console.Write($"{matr[i, j],10:F3}");
                }
                Console.WriteLine();
            }
            Console.WriteLine("------------------------------------");
            double min, max = double.MinValue;
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                min = matr[i, 0];
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    if (matr[i, j] < min)
                        min = matr[i, j];
                }
                if (min > max)
                    max = min;
                Console.WriteLine($"Найменший елемент рядка №{i + 1} = {min:F3}");
            }
            Console.WriteLine("------------------------------------");
            Console.WriteLine($"Найбільший елемент серед найменших: {max:F3}");
            Console.WriteLine("------------------------------------");

            double tmp;
            int x = 0;
            for (int i = 1; i < matr.GetLength(0); i += 2)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    if (matr[i, j] < 0.0 && j == 0)
                    {
                        x++;
                        continue;
                    }
                    if (j > 0 && matr[i, j] < 0.0 && matr[i, j - 1] > 0.0)
                    {
                        for (int k = j; k > x; k--)
                        {
                            tmp = matr[i, k];
                            matr[i, k] = matr[i, k - 1];
                            matr[i, k - 1] = tmp;
                        }
                        x++;
                    }
                    if (j > 0 && matr[i, j] < 0.0 && matr[i, j - 1] < 0.0)
                        x++;
                }
                x = 0;
            }

            for (int i = 0; i < matr.GetLength(0); i++)
            {
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    Console.Write($"{matr[i, j],10:F3}");
                }
                Console.WriteLine();
            }
        }
    }
}
