﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays1DWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double[] arr;

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                       System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            decimal n = numericUpDownCount.Value;
            int count = (int)(n);
            arr = new double[count];
            dataGridViewArray.RowCount = 1;
            dataGridViewArray.ColumnCount = count;
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = -15.6 + rnd.NextDouble() * (53.3 + 15.6);
                dataGridViewArray[i, 0].Value = Math.Round(arr[i], 1);
                dataGridViewArray.Columns[i].HeaderText = i.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double sum = 0.0;
            for (int i = 0; i < arr.Length; i++)
            {
                if ((Math.Abs(Math.Round(arr[i], 1)) - Math.Abs(Math.Truncate(arr[i]))) >= 0.5)
                    sum += Math.Abs(Math.Round(arr[i], 1));
            }
            textBoxSum.Text = sum.ToString("F1");

            double tmp;
            bool isEnabled;
            do
            {
                isEnabled = false;
                for (int i = arr.Length / 2; i < arr.Length - 1; i++)
                {
                    if (arr[i] < arr[i + 1])
                    {
                        tmp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = tmp;
                        isEnabled = true;
                    }
                }
            } while (isEnabled);

            for (int i = 0; i < arr.Length; i++)
                dataGridViewArray[i, 0].Value = Math.Round(arr[i], 1);
        }

        private void dataGridViewMatrix_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == -1 && e.RowIndex > -1)
            {
                e.PaintBackground(e.CellBounds, true);
                using (SolidBrush br = new SolidBrush(Color.Black))
                {
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(e.RowIndex.ToString(),
                      e.CellStyle.Font, br, e.CellBounds, sf);
                }
                e.Handled = true;
            }
        }
    }
}
